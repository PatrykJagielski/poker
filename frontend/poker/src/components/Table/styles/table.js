import styled from "styled-components";

export const Container = styled.div`
  display: grid;
  width: 98vw;
  height: 98vh;
  padding-top: 20px;
`

export const Table = styled.div`
  justify-self: center;
  align-self: center;
  max-width: 960px;
  width: 100%;
  max-height: 460px;
  height: 100%;
  background-color: #4d2712;
  border-radius: 170px;
  position: relative;
  display: grid;
`

export const OutsideRing = styled.div`
  max-width: 900px;
  max-height: 360px;
  width: 100%;
  height: 100%;
  /* background: #36aa25; */
  background: radial-gradient(circle, #36aa25 -30%, #073602 90%);
  position: relative;
  border-radius: 150px;
  justify-self: center;
  align-self: center;
  border: 5px #c29f41 solid;
  justify-content: center;
`

export const Player1 = styled.div`
  position: absolute;
  height: 100px;
  width: 105px;
  background: white;
  bottom: -100px;
  left: 400px;
`

export const Card1 = styled.div`
  position: relative;
  top: 105px;
  background: red;
  width: 50px;
  height: 75px;
`

export const Card2 = styled.div`
  position: relative;
  top: 30px;
  left: 55px;
  background: red;
  width: 50px;
  height: 75px;
`

export const GameBoard = styled.div`
  align-items: center;
  justify-content: center;
  column-gap: 5px;
  display: flex;
  
`

export const Player2 = styled.div`
  position: absolute;
  height: 100px;
  width: 105px;
  background: white;
  bottom: -100px;
  left: 125px;
`

export const Navigation = styled.div`
  display: flex;
  max-height: 50px;
  margin-top: 200px;
  
`

export const TableButton = styled.button`
  margin-left: 10px;
  height: 40px;
`