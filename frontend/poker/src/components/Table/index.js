import React, { useEffect, useState } from "react";
import { w3cwebsocket as W3CWebSocket } from "websocket";
import { Container, Table, OutsideRing, Player1, Player2, Card1, Card2, GameBoard, Navigation, TableButton } from "./styles/table"
import { useParams } from "react-router-dom";

export default function TableContainer({ children, ...restProps }) {
  const [hand, setHand] = useState(['??', '??']);
  const [flop, setFlop] = useState(['??', '??', '??']);
  const [turn, setTurn] = useState('');
  const [river, setRiver] = useState('');
  const [client, setClient] = useState();

  let params = useParams();

  useEffect(() => {

    const clientW3C = new W3CWebSocket('ws://localhost:8000/ws/table/'+ params.uuid +'/?jwt=' + localStorage.getItem('access_token'))
    setClient(clientW3C)

    clientW3C.onopen = () => {
      console.log("Connected to WS")
    }

    clientW3C.onmessage = (message) => {
      const dataFromServer = JSON.parse(message.data)
      console.log(dataFromServer)
      if(dataFromServer.hand) setHand(dataFromServer.hand)
      if(dataFromServer.flop) setFlop(dataFromServer.flop);
      if(dataFromServer.turn) setTurn(dataFromServer.turn);
      if(dataFromServer.river) setRiver(dataFromServer.river);
    }

    return () => {
      clientW3C.close()
    }

  }, [params])

  const sendMessage = (event) => {
    client.send(JSON.stringify({
      'message': event.target.value
    }))
  }

  return <Container {...restProps}>
      <Table>
        <OutsideRing>
          <Player1>
            <Card1> {hand[0]} </Card1>
            <Card2> {hand[1]} </Card2>
          </Player1>
          <Player2>
            <Card1> ?? </Card1>
            <Card2> ?? </Card2>
          </Player2>
          {children} 
          {' ' + params.uuid}
          <GameBoard>
            <Card1> {flop[0]} </Card1>
            <Card1> {flop[1]} </Card1>
            <Card1> {flop[2]} </Card1>
            {turn ? <Card1> {turn} </Card1> : null}
            {river ? <Card1> {river} </Card1> : null}
          </GameBoard>
        </OutsideRing>
      </Table>
      <Navigation>
        <TableButton onClick={sendMessage} value='pass'>Pass</TableButton>
        <TableButton onClick={sendMessage} value='check'>Check</TableButton>
        <TableButton onClick={sendMessage} value='raise'>Raise</TableButton>
      </Navigation>
    </Container>
}

