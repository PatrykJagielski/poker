import styled from "styled-components";

export const Container = styled.div`
  position: fixed;
  padding: 10px 10px 10px 10px;
  top: 0;
  height: 40px;
  background: grey;
  width: 100%;
  display: flex;
  align-items: center;
  justify-content: center;
  column-gap: 50px;
  a {
   font-size: 24px;
   color: #e0e0e0;
   text-decoration: none;
  }
  a:hover {
    color: #f0a8a8;
  }
`