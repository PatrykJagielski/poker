import React, { useState, useEffect } from 'react';
import { Container } from './styles/navbar';
import { Link } from 'react-router-dom';

export default function NavBar (props){
  const [isLoggedIn, setIsLoggedIn] = useState(false)

  useEffect(() => {
    localStorage.getItem('access_token') ? setIsLoggedIn(true) : setIsLoggedIn(false)
  }, [])
  
  const handleLogOut = () => {
    localStorage.removeItem('access_token')
    localStorage.removeItem('refresh_token')
  }


  return (
  <Container>
    <Link to='/'> Home </Link>
    {
      isLoggedIn ? 
      <Link to='/signin' onClick={handleLogOut}> Logout </Link> 
      : 
      <React.Fragment>
        <Link to='/signin'> SignIn </Link>
        <Link to='/signin'> SignIn </Link>
      </React.Fragment>
    }
  </Container>
  )
}