import React from "react";
import Footer from '../components/footer'

export function FooterContainer() {
  return (
    <Footer>
      Copyright &copy; 2022, Patryk Jagielski
    </Footer>
  )
}