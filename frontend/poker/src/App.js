import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';
import Home from './pages/home';
import SignIn from './pages/singin';
import SignUp from './pages/signup';
import Table from './pages/table';

function App() {
  return (
    <Router>
      <Routes>
        <Route exact path='/' element={<Home/>} />
        <Route exact path='/signin' element={<SignIn/>} />
        <Route exact path='/signup' element={<SignUp/>} />
        <Route path='/table/:uuid/' element={<Table/>} />
      </Routes>
    </Router>
  );
}

export default App;
