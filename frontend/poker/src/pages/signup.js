import React, {useState} from 'react'
import {useNavigate} from 'react-router-dom'
import NavBar from '../components/NavBar'
import { FooterContainer } from '../containers/footer'
import Form from "../components/Form";

export default function SignUp() {

  const [username, setUsername] = useState('');
  const [emailAddress, setEmailAddress] = useState('');
  const [password, setPassword] = useState('');
  const [error, setError] = useState('');

  const isInvalid = username === '' || password === '' || emailAddress === '';

  let navigate = useNavigate();

  const handleSignup = (event) => {
    event.preventDefault()
    fetch('http://localhost:8000/api/user/create/', {
      method: "POST",
      mode: 'cors',
      headers: {
        "Content-Type": "application/json",
      },
      referrerPolicy: 'no-referrer',
      body: JSON.stringify({
        'email': emailAddress,
        'password': password,
        'user_name': username
      }),
    }).then( response => {
      if(!response.ok){
        return response.json()
      }
      else{
        navigate('/signin')
      }
    })
    .then(data => {
      if(data) {
        setError(data[0])
      }
    })
    .catch((error) => {
      console.log(error);
    });
  };

  return (
    <div className="App">
      <NavBar />
        <Form>
          <Form.Title>Sign Up</Form.Title>
          {error && <Form.Error>{error}</Form.Error>}

          <Form.Base onSubmit={handleSignup} method="POST">
          <Form.Input
              placeholder="Username"
              value={username}
              onChange={({ target }) => setUsername(target.value)}
            />
          <Form.Input
              placeholder="Email address"
              value={emailAddress}
              onChange={({ target }) => setEmailAddress(target.value)}
            />
            <Form.Input
              type="password"
              value={password}
              autoComplete="new-password"
              placeholder="Password"
              onChange={({ target }) => setPassword(target.value)}
            />

            <Form.Submit disabled={isInvalid} type="submit" data-testid="sign-up">
              Sign Up
            </Form.Submit>
            
          </Form.Base>


        </Form>
      <FooterContainer />
    </div>
  );
}