import React, {useState} from 'react'
import { useNavigate } from 'react-router-dom'
import NavBar from '../components/NavBar'
import { FooterContainer } from '../containers/footer'
import Form from "../components/Form";

export default function SignUp() {

  const [emailAddress, setEmailAddress] = useState('');
  const [password, setPassword] = useState('');
  const [error, setError] = useState('');

  const isInvalid = password === '' || emailAddress === '';
  let navigate = useNavigate();

  const handleSignIn = (event) => {
    event.preventDefault()
    fetch('http://localhost:8000/api/token/', {
      method: "POST",
      mode: 'cors',
      headers: {
        "Content-Type": "application/json",
      },
      referrerPolicy: 'no-referrer',
      body: JSON.stringify({
        'email': emailAddress,
        'password': password
      }),
    }).then( response => {
      if(!response.ok){
        setError('Wrong credentials')
      }
      else{
        return response.json()
      }
    })
    .then(data => {
      if(data){
        localStorage.setItem('access_token', data.access);
        localStorage.setItem('refresh_token', data.refresh);
        navigate('/')
      }
    })
    .catch((error) => {
      console.log(error);
    });
  };

  return (
    <div className="App">
      <NavBar />
        <Form>
          <Form.Title>Sign In</Form.Title>
          {error && <Form.Error>{error}</Form.Error>}

          <Form.Base onSubmit={handleSignIn} method="POST">
          <Form.Input
              placeholder="Email address"
              value={emailAddress}
              onChange={({ target }) => setEmailAddress(target.value)}
            />
            <Form.Input
              type="password"
              value={password}
              autoComplete="new-password"
              placeholder="Password"
              onChange={({ target }) => setPassword(target.value)}
            />

            <Form.Submit disabled={isInvalid} type="submit" data-testid="sign-up">
              Sign Up
            </Form.Submit>
            
          </Form.Base>


        </Form>
      <FooterContainer />
    </div>
  );
}