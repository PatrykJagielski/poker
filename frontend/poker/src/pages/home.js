import { useNavigate } from "react-router-dom"
import '../App.css';
import React, { useState } from 'react'
import NavBar from '../components/NavBar'
import { FooterContainer } from '../containers/footer'
import Form from "../components/Form";

export default function Home() {

  const [tableID, setTableID] = useState('');
  const navigate = useNavigate();
  const isInvalid = tableID === '';

  const handleJoinTable = (event) => {
    navigate('/table/' + tableID)
  }

  return (
    <div className="App">
      <NavBar />
        <Form> 
          <Form.Title>Join to table</Form.Title>
          <Form.Base method="POST"></Form.Base>
          <Form.Input
              placeholder="table ID"
              value={tableID}
              onChange={({ target }) => setTableID(target.value)}
            />
          <Form.Submit onClick={handleJoinTable} disabled={isInvalid} type="submit" data-testid="table-id">
              Join Table
          </Form.Submit>
          <Form.Title>OR</Form.Title>
          <Form.Submit type="submit" data-testid="create-table">
              Create Table
          </Form.Submit>
        </Form>
      <FooterContainer />
    </div>
  )
}