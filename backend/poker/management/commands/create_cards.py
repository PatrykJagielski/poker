from django.core.management.base import BaseCommand, CommandError
from poker.models import Card

class Command(BaseCommand):
    help = 'Create cards for poker'

    def handle(self, *args, **options):
        ranks = ['2', '3', '4', '5', '6', '7', '8', '9', '10', 'J', 'Q', 'K', 'A']
        suits = ['♣', '♦', '♥', '♠']

        for suit in suits:
            for rank in ranks:
                Card.objects.create(rank=rank, suit=suit)
        
        # for poll_id in options['poll_ids']:
        #     try:
        #         poll = Poll.objects.get(pk=poll_id)
        #     except Poll.DoesNotExist:
        #         raise CommandError('Poll "%s" does not exist' % poll_id)

        #     poll.opened = False
        #     poll.save()

        self.stdout.write(self.style.SUCCESS('Successfully run command'))