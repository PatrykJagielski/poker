from django.urls import re_path, path

from .consumers import TableConsumer


websocket_urlpatterns = [
    path('ws/table/<uuid:room_name>/', TableConsumer.as_asgi()),
]