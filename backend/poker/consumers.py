from email import message
import json
from os import remove
from channels.generic.websocket import AsyncWebsocketConsumer
from channels.db import database_sync_to_async
from django.core import serializers

from .models import Table, Card


class TableConsumer(AsyncWebsocketConsumer):
    async def connect(self):
        self.user = self.scope['user']
        self.user.status = 'connected'
        await database_sync_to_async(self.user.save)()

        self.room_name = self.scope['url_route']['kwargs']['room_name']
        self.room_group_name = 'table_%s' % self.room_name

        self.table, created = await self.get_or_create_table(self.room_name)

        await self.pass_hand(self.user)

        # Join room group
        await self.channel_layer.group_add(
            self.room_group_name,
            self.channel_name
        )
        await self.channel_layer.group_add(
            self.user.user_name,
            self.channel_name
        )

        await self.accept()

        await self.add_player_to_table(self.user)

        num_of_players = await database_sync_to_async(self.table.players.count)()
        if num_of_players >= 2:
            await self.channel_layer.group_send(
                self.room_group_name,
                {
                    'type': 'game_start',
                    'flop': await self.get_flop(),
                }
            )
            
            for player in await self.get_players():
                await self.channel_layer.group_send(
                    player.user_name,
                    {
                        'type': 'send_dict',
                        'event': {
                            'hand': await self.get_user_hand(player),
                        }
                    }
                )

    @database_sync_to_async
    def get_players(self):
        return [player for player in  self.table.players.all()]

    @database_sync_to_async
    def add_player_to_table(self, user):
        self.table.players.add(user)

    @database_sync_to_async
    def remove_player_from_table(self, user):
        self.table.players.remove(user)

    @database_sync_to_async
    def get_flop(self):
        return [c.rank + c.suit for c in self.table.pack_of_cards.all().order_by('?')[:3]]

    @database_sync_to_async
    def get_user_hand(self, user):
        return [h.rank + h.suit for h in user.hand.all()]

    async def disconnect(self, close_code):
        await self.remove_player_from_table(self.user)
        # Leave room group
        await self.channel_layer.group_discard(
            self.room_group_name,
            self.channel_name
        )

    # Receive message from WebSocket
    async def receive(self, text_data):
        text_data_json = json.loads(text_data)
        message = text_data_json['message']

        if message == 'pass':
            await self.channel_layer.group_send(
                self.room_group_name,
                {
                    'type': 'info_message',
                    'message': message
                }
            )
        elif message == 'check':
            self.user.status = 'check'
            await database_sync_to_async(self.user.save)()
            if all([player.status == 'check' for player in await self.get_players()]):
                await self.channel_layer.group_send(
                    self.room_group_name,
                    {
                        'type': 'send_dict',
                        'event': {'turn': await self.get_card()}
                    }
                )
        elif message == 'raise':
            await self.channel_layer.group_send(
                self.room_group_name,
                {
                    'type': 'send_dict',
                    'event': {'river': self.get_card()}
                }
            )

    @database_sync_to_async
    def get_card(self):
        card = self.table.pack_of_cards.all().order_by('?').first()
        return card.rank + card.suit

    @database_sync_to_async
    def pass_hand(self, user):
        user.hand.set(self.table.pack_of_cards.all().order_by('?')[:2])

    @database_sync_to_async
    def get_or_create_table(self, uuid):
        return Table.objects.get_or_create(id=uuid)

    # Receive message from room group
    async def chat_message(self, event):
        message = event['message']
        card = event['cards']

        # Send message to WebSocket
        await self.send(text_data=json.dumps({
            'message': message,
            'card': card
        }))

    async def info_message(self, event):
        message = event['message']

        await self.send(text_data=json.dumps({
            'user': self.user.user_name,
            'message': message
        }))

    async def game_start(self, event):
        cards = event['flop']

        await self.send(text_data=json.dumps({
            'flop': cards,
        }))

    async def send_dict(self, event):
        await self.send(text_data=json.dumps(event['event']))