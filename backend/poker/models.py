from enum import unique
from urllib.parse import DefragResult
from uuid import uuid4
import uuid

from django.db import models
from django.conf import settings


class Table(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4)
    pool = models.PositiveIntegerField(default=0)
    round = models.PositiveIntegerField(default=0)
    pack_of_cards = models.ManyToManyField("Card")
    flop = models.ManyToManyField("Card", related_name='flop', null=True, blank=True)
    turn = models.ForeignKey("Card", on_delete=models.SET_NULL, null=True, blank=True, related_name='turn')
    river = models.ForeignKey("Card", on_delete=models.SET_NULL, null=True, blank=True, related_name='river')
    players = models.ManyToManyField("users.NewUser", default=None, null=True, blank=True)


class Card(models.Model):
    rank = models.CharField(max_length=2, null=True)
    suit = models.CharField(max_length=1, null=True)

    class Meta:
        unique_together = [['rank', 'suit']]

    def __str__(self):
        if not self.rank or not self.suit:
            return ''
        return self.rank + self.suit

