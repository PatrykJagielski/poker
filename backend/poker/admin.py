from django.contrib import admin
from .models import Table, Card

admin.site.register(Table)
admin.site.register(Card)