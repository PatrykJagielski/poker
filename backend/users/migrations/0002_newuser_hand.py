# Generated by Django 4.0 on 2022-02-13 01:09

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('poker', '0001_initial'),
        ('users', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='newuser',
            name='hand',
            field=models.ManyToManyField(default=None, to='poker.Card'),
        ),
    ]
