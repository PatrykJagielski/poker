# Generated by Django 4.0 on 2022-02-21 00:30

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0002_newuser_hand'),
    ]

    operations = [
        migrations.AddField(
            model_name='newuser',
            name='status',
            field=models.CharField(blank=True, default=None, max_length=128, null=True),
        ),
    ]
