from rest_framework.views import APIView
from rest_framework.permissions import AllowAny
from rest_framework.response import Response
from rest_framework import status, serializers

from .serializers import CustomUserSerializer
from .models import NewUser


class CustomUserCreate(APIView):
    permission_classes = [AllowAny]

    def post(self, request, format='json'):
        serializer = CustomUserSerializer(data=request.data)
        if serializer.is_valid():
            username = serializer.validated_data.get('user_name')
            email = serializer.validated_data.get('email')
            if NewUser.objects.filter(user_name__exact=username).exists():
                raise serializers.ValidationError("Name already exists!")
            if NewUser.objects.filter(email__exact=email).exists():
                raise serializers.ValidationError("Email already exists!")
            user = serializer.save()
            if user:
                json = serializer.data
                return Response(json, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)